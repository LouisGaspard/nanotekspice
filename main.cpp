/*
** EPITECH PROJECT, 2019
** nano
** File description:
** nano
*/

#include "Nanotekspice.hpp"

int main(int ac, char **av)
{
    if (ac <= 1) {
        std::cerr << "Usage: ./nanotekspice [file.nts] ([input=value] [...])" << std::endl;
        return (84);
    }
    try {
        nts::Nanotekspice *nanotekspice = new nts::Nanotekspice(ac, av);
        delete nanotekspice;
    } catch (nts::NtsError const &e) {
        std::cerr << e.what() << std::endl;
    }
    return (0);
}
