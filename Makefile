NAME	= nanotekspice

CC	= g++

RM	= rm -f

SRCS	= ./main.cpp 			\
	  ./src/Nanotekspice.cpp	\
	  ./src/Circuit.cpp		\
	  ./src/Parser.cpp		\
	  ./src/Errors.cpp

OBJS	= $(SRCS:.cpp=.o)

CPPFLAGS = -I ./include/
CPPFLAGS += -Wall -Wextra

all: $(NAME)

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME) $(LDFLAGS)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
