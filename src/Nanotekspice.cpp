/*
** EPITECH PROJECT, 2019
** nano
** File description:
** nano
*/

#include "Nanotekspice.hpp"
#include "Errors.hpp"

nts::Nanotekspice::Nanotekspice(int ac, char **av) {
  this->_ac = ac;
  this->_filename = this->_filename.assign(av[1]);

  for (int i = 2; i < ac; i++) {
    std::string argv(av[i]);
    this->_av.push_back(argv);
  }
  try {
    this->_circuit = new nts::Circuit(this->_filename, this->_av);
  } catch (nts::NtsError const &e) {
    throw (e);
  }
}

nts::Nanotekspice::~Nanotekspice(void) {
  if (this->_circuit != NULL)
    delete this->_circuit;
}

void nts::Nanotekspice::exit(void) {

}

void nts::Nanotekspice::display(void) {

}

void nts::Nanotekspice::simulate(void) {
  if (this->_circuit != NULL)
    this->_circuit->preStart();
}

void nts::Nanotekspice::loop(void) {

}

void nts::Nanotekspice::dump(void) {

}

std::string nts::Nanotekspice::getFilename(void) {
  return (this->_filename);
}
