/*
** EPITECH PROJECT, 2019
** nano
** File description:
** nano
*/

#include "Errors.hpp"

nts::NtsError::NtsError(std::string const &e) throw()
{
  this->_error = e;
}

nts::NtsError::NtsError(void) throw()
{
  this->_error = "";
}

nts::NtsError::~NtsError(void) throw() {}

const char *nts::NtsError::what(void) const throw()
{
  return (this->_error.c_str());
}

nts::FileError::FileError(std::string const &e) throw() : NtsError(e) {}

nts::FileError::~FileError(void) throw() {}
