/*
** EPITECH PROJECT, 2019
** OOP
** File description:
** Created by louis,
*/

#include "Parser.hpp"

nts::Parser::Parser(std::vector<std::string> av) {
    this->_av = av;
}

nts::Parser::~Parser() {

}

std::vector<std::string> nts::Parser::getArg() {
    return (this->_av);
}

void nts::Parser::checkLine(std::string line) {
    std::cout << "line -> : " << line << std::endl;
}