/*
** EPITECH PROJECT, 2019
** nano
** File description:
** nano
*/

#include "Circuit.hpp"
#include "Errors.hpp"
#include "Parser.hpp"

nts::Circuit::Circuit(std::string filename, std::vector<std::string> av) {
    this->_filename = filename;

    size_t ext_pos = filename.find_last_of(".");
    if (filename.substr(ext_pos, filename.size()) != ".nts")
        throw (nts::FileError("File error: bad file format '" +
                              filename.substr(ext_pos, filename.size()) + "'"));

    this->_parser = new Parser(av);
    std::ifstream ifs(filename);
    std::string line;
    if (ifs.is_open()) {
        while (std::getline(ifs, line))
            this->_parser->checkLine(line);
    } else {
        throw (nts::FileError("File error: can't open file '" + filename + "'"));
    }
}

nts::Circuit::~Circuit(void) {

}

void nts::Circuit::preStart() {

}
