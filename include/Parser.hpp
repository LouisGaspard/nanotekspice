/*
** EPITECH PROJECT, 2019
** OOP
** File description:
** Created by louis,
*/

#ifndef OOP_PARSER_HPP
#define OOP_PARSER_HPP

#include <vector>
#include <string>
#include <iostream>

namespace nts {
    class Parser {
    public:
        Parser(std::vector<std::string>);
        ~Parser();

        std::vector<std::string> getArg();
        void checkLine(std::string line);
    private:
        std::vector<std::string> _av;
    };
}

#endif //OOP_PARSER_HPP