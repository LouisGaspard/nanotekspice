/*
** EPITECH PROJECT, 2019
** nano
** File description:
** nano
*/

#ifndef _CIRCUIT_HPP
#define _CIRCUIT_HPP

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include "Parser.hpp"

namespace nts {
  class Circuit {
  public:
    Circuit(std::string, std::vector<std::string>);
    ~Circuit(void);

    void preStart();
  private:
    std::string _filename;
    Parser *_parser;
  };
}

#endif