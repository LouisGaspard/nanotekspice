/*
** EPITECH PROJECT, 2019
** nano
** File description:
** nano
*/

#ifndef _NANOTEKSPICE_HPP
#define _NANOTEKSPICE_HPP

#include <iostream>
#include <string>
#include <vector>

#include "Errors.hpp"
#include "Circuit.hpp"

namespace nts {
  class Nanotekspice {
  public:
    Nanotekspice(int, char **);
    ~Nanotekspice(void);

    std::string getFilename(void);
  private:
    int _ac;
    std::string _filename;
    std::vector<std::string> _av;
    nts::Circuit *_circuit;

    void exit(void);
    void display(void);
    void simulate(void);
    void loop(void);
    void dump(void);
  };
}

#endif