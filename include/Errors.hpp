/*
** EPITECH PROJECT, 2019
** nano
** File description:
** nano
*/

#ifndef _ERRORS_HPP
#define _ERRORS_HPP

#include <string>
#include <exception>

namespace nts {
  class NtsError : public std::exception {
  public:
    NtsError(std::string const &e) throw();
    NtsError(void) throw();
    virtual ~NtsError(void) throw();
    virtual const char *what(void) const throw();
  private:
    std::string _error;
  };

  class FileError : public NtsError {
  public:
    FileError(std::string const &e) throw();
    virtual ~FileError(void) throw();
  };
}

#endif